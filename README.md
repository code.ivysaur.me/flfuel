# flfuel

![](https://img.shields.io/badge/written%20in-C%2B%2B%20%28FLTK%29%2C%20PHP-blue)

A fuel tracking program.

After refilling your vehicle, log the odometer reading to keep track of fuel efficiency.

## Features

- Automatic graph generation via `fltkmchart`
- Handles incomplete data (zero odo/fuel/date values) as gracefully as possible
- Simple plaintext `.flf` file format suitable for revision control
- Alternative ealier PHP version, for prototyping graph generation with `mchart`

## Changelog

2015-06-01: 1.1
- Fix an issue with roundtrip persistence
- [⬇️ flfuel-1.1-src.zip](dist-archive/flfuel-1.1-src.zip) *(19.14 KiB)*
- [⬇️ flfuel-1.1-bin.zip](dist-archive/flfuel-1.1-bin.zip) *(156.04 KiB)*


2015-06-01: 1.0
- Initial public release of FLTK version
- Breaking change to file format (now `\n` instead of `\t` separated)
- [⬇️ flfuel-1.0-src.zip](dist-archive/flfuel-1.0-src.zip) *(18.45 KiB)*
- [⬇️ flfuel-1.0-bin.zip](dist-archive/flfuel-1.0-bin.zip) *(156.03 KiB)*


2015-06-01: 0.1
- Initial public release of PHP version
- [⬇️ flfuel-0.1.zip](dist-archive/flfuel-0.1.zip) *(1.51 KiB)*


2015-05-24:
- Feature: Log efficiency, make KPL and L/100K charts

2014-07-07:
- Initial private release of PHP version
